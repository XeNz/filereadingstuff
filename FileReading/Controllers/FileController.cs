﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FileReading.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FileController : ControllerBase
    {
        private readonly ILogger<FileController> _logger;
        private readonly IFileService _fileService;

        public FileController(ILogger<FileController> logger, IFileService fileService)
        {
            _logger = logger;
            _fileService = fileService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var mappedAttachments = await ProcessAttachments(new List<string> {"test.txt"});
            // random test to see if we can print the contents of the file. this means the stream is usable and we can use it to upload it to another service for example
            var fileContent = await PrintFirstFileAsString(mappedAttachments);

            return Ok(fileContent);
        }

        private async Task<string> PrintFirstFileAsString(IEnumerable<DosAttachmentModel> mappedAttachments)
        {
            var firstFileSteam = mappedAttachments.First().FileStream;
            // gotta start at the start yo
            firstFileSteam.Seek(0, SeekOrigin.Begin);
            using var reader = new StreamReader(firstFileSteam);
            var fileContents = await reader.ReadToEndAsync();
            _logger.LogInformation(fileContents);
            return fileContents;
        }

        /// <summary>
        /// This method processes the attachments of a Dossier.
        /// The first attachment in the list is an overview of all Dossiers, so this one should be saved as the first attachment in MOP as well.
        /// The rest can be handled async
        /// </summary>
        /// <returns></returns>
        private async Task<IEnumerable<DosAttachmentModel>> ProcessAttachments(IEnumerable<string> filePaths)
        {
            //replace with the share url once this is no longer supposed to be local-only
            var path = Directory.GetCurrentDirectory();
            var attachmentTasks = filePaths.Select(x => DownloadFile(x, path));
            return await Task.WhenAll(attachmentTasks);
        }

        private async Task<DosAttachmentModel> DownloadFile(string file, string path)
        {
            var fullPath = _fileService.CombinePath(path, "LocalFiles", file);
            var stream = await _fileService.GetFileStream(fullPath);
            var contentType = await _fileService.GetContentType(fullPath);

            var seekableStream = new MemoryStream();
            await stream.CopyToAsync(seekableStream);
            return new DosAttachmentModel
            {
                FileStream = seekableStream,
                ContentType = contentType,
                FileName = Path.GetFileName(fullPath)
            };
        }
    }
}