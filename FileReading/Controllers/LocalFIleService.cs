using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.StaticFiles;

namespace FileReading.Controllers
{
    public class LocalFIleService : IFileService
    {
        private readonly IContentTypeProvider _fileTypeContentTypeProvider;

        public LocalFIleService(IContentTypeProvider fileTypeContentTypeProvider)
        {
            _fileTypeContentTypeProvider = fileTypeContentTypeProvider;
        }

        public string CombinePath(string fileRoot, string storeName, string filePath) => Path.Combine(fileRoot, storeName, filePath);

        public Task<string> GetContentType(string completeFilePath)
        {
            _fileTypeContentTypeProvider.TryGetContentType(completeFilePath, out var contentType);
            return Task.FromResult(contentType);
        }

        public Task<bool> FileExists(string completeFilePath) => Task.FromResult(File.Exists(completeFilePath));

        public Task<Stream> GetFileStream(string completeFilePath)
        {
            Stream stream = null;

            if (File.Exists(completeFilePath))
            {
                stream = File.OpenRead(completeFilePath);
            }

            return Task.FromResult(stream);
        }
    }

    public interface IFileService
    {
        Task<string> GetContentType(string completeFilePath);

        /// <summary>
        /// Checks whether or not a file exists
        /// </summary>
        /// <param name="completeFilePath"></param>
        /// <returns></returns>
        Task<bool> FileExists(string completeFilePath);

        /// <summary>
        /// Combines multiple parts of a path into a full path
        /// </summary>
        /// <param name="fileRoot"></param>
        /// <param name="storeName"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        string CombinePath(string fileRoot, string storeName, string filePath);

        /// <summary>
        /// Gets a file by its full path
        /// </summary>
        /// <param name="completeFilePath"></param>
        /// <returns></returns>
        Task<Stream> GetFileStream(string completeFilePath);
    }
}