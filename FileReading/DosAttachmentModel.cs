using System.IO;

namespace FileReading
{
    public class DosAttachmentModel
    {
        public Stream FileStream { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
    }
}